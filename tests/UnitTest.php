<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Entity\Dema;

class UnitTest extends TestCase
{
    public function testDema(): void
    {
        $dema = new Dema();
        $dema->setDema("dema");
        $this->assertTrue($dema->getDema() === "dema");
    }
}
