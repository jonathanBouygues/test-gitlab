<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class FunctionalTest extends WebTestCase
{
    public function testShouldDisplayDema(): void
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/dema');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Dema index');
    }

    public function testShouldDisplayForm(): void
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/dema/new');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Create new Dema');
    }

    public function testCreateDema(): void
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/dema/new');
        // Created client click on the button save
        $buttonCrawlerNode = $crawler->selectButton('Save');

        $form = $buttonCrawlerNode->form();
        // Create the data
        // Generation of unique id
        $uuid = uniqid();
        // Get the generated form
        $form = $buttonCrawlerNode->form([
            'dema[Dema]' => 'New dema' . $uuid,
        ]);
        // Created client submit the form
        $client->submit($form);

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('body', 'New dema' . $uuid);
    }
}
