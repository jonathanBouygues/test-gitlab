<?php

namespace App\Repository;

use App\Entity\Dema;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Dema|null find($id, $lockMode = null, $lockVersion = null)
 * @method Dema|null findOneBy(array $criteria, array $orderBy = null)
 * @method Dema[]    findAll()
 * @method Dema[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DemaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Dema::class);
    }

    // /**
    //  * @return Dema[] Returns an array of Dema objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Dema
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
