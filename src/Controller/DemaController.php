<?php

namespace App\Controller;

use App\Entity\Dema;
use App\Form\DemaType;
use App\Repository\DemaRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/dema")
 */
class DemaController extends AbstractController
{
    /**
     * @Route("/", name="dema_index", methods={"GET"})
     */
    public function index(DemaRepository $demaRepository): Response
    {
        return $this->render('dema/index.html.twig', [
            'demas' => $demaRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="dema_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $dema = new Dema();
        $form = $this->createForm(DemaType::class, $dema);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($dema);
            $entityManager->flush();

            return $this->redirectToRoute('dema_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('dema/new.html.twig', [
            'dema' => $dema,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="dema_show", methods={"GET"})
     */
    public function show(Dema $dema): Response
    {
        return $this->render('dema/show.html.twig', [
            'dema' => $dema,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="dema_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Dema $dema, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(DemaType::class, $dema);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('dema_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('dema/edit.html.twig', [
            'dema' => $dema,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="dema_delete", methods={"POST"})
     */
    public function delete(Request $request, Dema $dema, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete' . $dema->getId(), $request->request->get('_token'))) {
            $entityManager->remove($dema);
            $entityManager->flush();
        }

        return $this->redirectToRoute('dema_index', [], Response::HTTP_SEE_OTHER);
    }
}
