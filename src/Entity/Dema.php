<?php

namespace App\Entity;

use App\Repository\DemaRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DemaRepository::class)
 */
class Dema
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Dema;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDema(): ?string
    {
        return $this->Dema;
    }

    public function setDema(string $Dema): self
    {
        $this->Dema = $Dema;

        return $this;
    }
}
